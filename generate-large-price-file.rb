#!/usr/bin/env ruby
random_prices = (1..10_000)
  .to_a
  .map { |i| rand(50..800_000) }
  .sort

File.open('lots-of-prices.txt', 'w') do |file|
  random_prices.lazy.each_with_index do |price, index|
    file.write("product number #{index + 1}, #{price}\n")
  end
end
