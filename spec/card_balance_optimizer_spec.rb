require 'card_balance_optimizer'
require 'pry'

describe CardBalanceOptimizer do

  describe "#initialize" do
    let(:runner) { CardBalanceOptimizer.new }
    it "has readable attributes 'prices', 'balance', and 'num_gifts'" do
      expect(runner.respond_to?(:prices)).to be true
      expect(runner.respond_to?(:balance)).to be true
      expect(runner.respond_to?(:num_gifts)).to be true
    end
  end

  let(:paxos_prices) {[
    'Candy Bar, 500',
    'Paperback Book, 700',
    'Detergent, 1000',
    'Headphones, 1400',
    'Earmuffs, 2000',
    'Bluetooth Stereo, 6000'
  ]}

  context "using the Paxos test inputs (only 2 gifts)" do
    context "when the card balance is 2500" do
      let(:runner) {
        CardBalanceOptimizer.new(prices: paxos_prices, balance: 2500)
      }
      let(:result) { runner.execute }
      it "it returns the candy bar and earmuffs" do
        expect(result).to eq "Candy Bar, 500, Earmuffs, 2000"
      end
    end

    context "when the card balance is 2300" do
      let(:runner) {
        CardBalanceOptimizer.new(prices: paxos_prices, balance: 2300)
      }
      let(:result) { runner.execute }
      it "it returns the paperback book and headphones" do
        expect(result).to eq "Paperback Book, 700, Headphones, 1400"
      end
    end

    context "when the card balance is 10000" do
      let(:runner) {
        CardBalanceOptimizer.new(prices: paxos_prices, balance: 10000)
      }
      let(:result) { runner.execute }
      it "it returns the earmuffs and bluetooth stereo" do
        expect(result).to eq "Earmuffs, 2000, Bluetooth Stereo, 6000"
      end
    end

    context "when the card balance is 1100" do
      let(:runner) {
        CardBalanceOptimizer.new(prices: paxos_prices, balance: 1100)
      }
      let(:result) { runner.execute }
      it "it returns 'Not Possible'" do
        expect(result).to eq 'Not Possible'
      end
    end
  end # end of Paxos-provided test cases

  context "3 gifts" do
    context "using the prices in the paxos example" do
      context "when the target sum is 3500" do
        let(:runner) {
          CardBalanceOptimizer.new(
            prices: paxos_prices,
            balance: 3500,
            num_gifts: 3
          )
        }
        let(:result) { runner.execute }
        it "returns the best sum for 3 gifts" do
          expect(result).to eq "Candy Bar, 500, Detergent, 1000, Earmuffs, 2000"
        end
      end

      context "when the target sum is 4500" do
        let(:runner) {
          CardBalanceOptimizer.new(
            prices: paxos_prices,
            balance: 4500,
            num_gifts: 3
          )
        }
        let(:result) { runner.execute }
        it "returns the best sum for 3 gifts" do
          expect(result).to eq "Detergent, 1000, Headphones, 1400, Earmuffs, 2000"
        end
      end
    end
  end
end
