require 'price_file'

describe PriceFile do
  context "reading a simple price file" do
    let(:prices) { PriceFile.read('./spec/test-price-files/test-a.txt') }
    it "Test A: returns an array of prices with no extra newline characters or spaces" do
      expect(prices).to be_instance_of Array
      expect(prices).to eq(['Candy Bar, 500', 'Pen, 700', 'Shirt, 1100', 'Bento Box, 1900'])
    end
  end
end
