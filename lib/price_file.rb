class PriceFile
  def self.read(filename)
    # work on optimizing IO later
    IO.readlines(filename).map(&:chomp)
  end
end
