class CardBalanceOptimizer
  attr_reader :prices, :balance, :num_gifts

  def initialize(opts = {})
    if opts[:num_gifts] && opts[:num_gifts].to_i > 3
      raise Error.new('More than 3 hurts my head too much')
    end

    @prices = opts[:prices] || []
    @balance = opts[:balance] || 0
    @num_gifts = opts[:num_gifts] || 2
  end

  def execute
    if num_gifts == 3
      three_sum_solution
    else
      better_solution
    end
  end

  private

  def three_sum_solution
    best_diff = Float::INFINITY
    best_gifts = []

    prices.each_with_index do |base_item, index|
      left_index = index + 1
      right_index = prices.length - 1
      base_price = extract_price(base_item)
      while left_index < right_index
        left_item = prices[left_index]
        right_item = prices[right_index]
        total = base_price + extract_price(left_item) + extract_price(right_item)
        diff = balance - total

        if diff < 0
          right_index -= 1
        else
          if diff < best_diff
            best_diff = diff
            best_gifts = [base_item, left_item, right_item]
            break if diff == 0
            left_index += 1
          else
            # if we get here we've seen the closest match we'll see
            break
          end
        end
      end
    end

    if best_gifts.length > 0
      return best_gifts.join(', ')
    else
      return 'Not Possible'
    end
  end

  def better_solution
    best_diff = Float::INFINITY
    best_gifts = []
    left_index = 0
    right_index = prices.length - 1
    while left_index < right_index
      left_item = prices[left_index]
      right_item = prices[right_index]
      total = extract_price(left_item) + extract_price(right_item)
      diff = balance - total
      # the main strategy is figuring out if we should move our right pointer
      # to the left (from more expensive to less expensive) or the left pointer
      # to the right (from less expensive to more expensive)
      if diff < 0
        right_index -= 1
      else
        if diff < best_diff
          best_diff = diff
          best_gifts = [ left_item, right_item ]
          if diff == 0
            break
          end

          left_index += 1
        else
          # break out of the loop b/c we've already seen the best match
          break
        end
      end
    end

    if best_gifts.length > 0
      return best_gifts.join(', ')
    else
      return 'Not Possible'
    end
  end

  def brute_force_solution
    best_diff = Float::INFINITY
    gifts = []

    # brute force solution first
    # nested loop, so O(n^2)

    prices.length.times do |i|
      prices.length.times do |j|
        next if i == j
        item_one = prices[i]
        item_two = prices[j]
        price = extract_price(item_one) + extract_price(item_two)
        if (price <= balance) && (balance - price < best_diff)
          best_diff = balance - price
          gifts = [item_one, item_two]
        end
      end
    end

    if gifts.length > 0
      gifts.join(', ')
    else
      'Not Possible'
    end
  end

  def extract_price(item)
    item.split(', ').last.to_i
  end
end
