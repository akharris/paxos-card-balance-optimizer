# Paxos Code Challenge 2

Problem: You have a gift card for X balance and you want to buy presents for
two friends. You should spend as close to the full balance of the card as
possible. You have a list of items sorted by their prices. Write a program
that accomplishes this goal.

## Assumptions
- The list of items is sorted in ascending order by the price
- There are no duplicates in the list
- The value of the item is in cents ($10 is represented as 1000)

## Setup

Here are the steps to pull this repo down and try it out.

Install some dependencies

- have [bundler](https://bundler.io/) installed
- have [rbenv](https://github.com/rbenv/rbenv#installation) or [rvm](https://rvm.io/) installed and set the ruby version to 2.2.5
  - for rbenv: `rbenv install 2.2.5` then `rbenv local`
  - for rvm: `rvm install 2.2.5` then `rvm 2.2.5`

Pull down the repo and run the tests

- `git clone git@gitlab.com:akharris/paxos-card-balance-optimizer.git`
- `cd paxos-card-balance-optimizer`
- `bundle install`
- `bundle exec rspec spec`

There are tests for all of the use cases described in the PDF containing
the assignment. There are also test cases for the bonus section for 3 gifts.

## Usage

If you can use the card balance command line utility. The command line utility
has a help flag that explains the API and shows a few examples.

```
❯ ./find-gifts help
    Welcome to the card-balance-optimizer command line utility!
    Usage: find-gifts [--help] [price-filename] [card balance] [number-of-gifts]

    Examples:
      - find-gifts prices.txt 2500
      - find-gifts prices.txt 1700
      - find-gifts amazing-products.txt 4500
      - find-gifts more-products.txt 2700 3
```

If you want to use the card balance optimizer on the command line you can
use the `prices.txt` file given by the assignment. You can also generate a
random prices list file and run the program against that list.

```
# using prices.txt
❯ ./find-gifts prices.txt 1600
Candy Bar, 500, Detergent, 1000

❯ ./find-gifts prices.txt 3400 3
Candy Bar, 500, Paperback Book, 700, Earmuffs, 2000

# using lots-of-prices.txt
❯ ./find-gifts lots-of-prices.txt 7418
product number 7, 704, product number 71, 6712
```

If you want to generate a new 'lots-of-prices.txt' file you can run ./generate-large-price-file.rb

## Big O
The Big O notation of the card balance optimizer for 2 gifts is linear or O(n).
There is one loop for that solution.

The Big O notation for 3 gifts is quadratic or O(n^2). There is a nested loop
for that solution.

Bonus Add-On:

- [x] make the program capable of accepting 3 gifts
- [x] optimize the solution for large files (i.e. no O(2^n) nonsense)
